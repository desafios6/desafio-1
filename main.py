def add_item(item, item_array):
    try:
        item_position = item_array.index(item)
        print("uma cotação com esse título já existe na lista!")
    except:
        item_array.append(item)
        print("nova cotação adicionada com sucesso!")


def remove_item(item, item_array):
    try:
        print(item_array.pop(item_array.index(item)) + " removido com sucesso!")
    except:
        print("Não há uma cotação com esse nome na lista")


def change_title(item, item_array, new_title):
    try:
        item_array[item_array.index(item)] = new_title
        print("título alteradocom sucesso!")
    except:
        print("Essa cotação não foi encontrada")

